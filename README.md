# surface_area

surface_area is a script to calculate the effective surface area available on the respective analysis scale.

## Usage

```python
from tabulate import tabulate
from src import read, plot, calculate

f_name = 'Probe1_32um.mi'

topography, general_settings = read.read_topography(f_name)
```
Reads the file stored in directory "data".

```python
surf_area_linear, enlargement_factor_linear = calculate.surface_area(topography, general_settings)
surf_area_triangular, enlargement_factor_triangular = calculate.surface_area_triangular(topography, general_settings)
```
Two different approaches to calculate the effective surface area:
- The linear approach is more rough and assumes the heigth differences between two cells as additional length. Additional length multiplied by cell widths equals the additional surface area. Projected surface area is added afterwards.
- The triangular approach assumes triangular surfaces with edges on the cell positions. The area of the triangles equals the effective surface area. Much more realistic approach but takes extremly long to loop over Dataframe elements (TODO in future).

```python
print(f'\nAnalyzed {f_name}:')
print(tabulate([['Linear', f'{surf_area_linear:5g}', f'{enlargement_factor_linear * 100:5g}'],
                ['Triangular', f'{surf_area_triangular:5g}', f'{enlargement_factor_triangular * 100:5g}']],
               headers=['Approach', 'Surf Area [µm^2]', 'Enlargement Factor [%]'], tablefmt='orgtbl'))
print(f'The projected area is {general_settings["width"]*general_settings["height"]:5g} µm^2.')
```

Resumes the results.

```python
plot.plot_contourf(topography, general_settings)
```
Plots the matrix as a contourf plot.

## License
Not yet licensed.
