import src.read
import src.plot
import src.calculate
from tabulate import tabulate

#f_name = 'raman_2SD_sinker.txt'      # 2.2284
#f_name = 'raman_polished_1.txt'      # 1.30106
#f_name = 'raman_polished_2.txt'      # 1.24426
#f_name = 'raman_rough_20_1.txt'      # 1.85267
#f_name = 'raman_rough_20_2.txt'      # 1.85267
f_name = 'raman_rough_50_1.txt'      # 2.01073
#f_name = 'afm_Probe1_32um.mi'        #
#f_name = 'wli_FP.txt'                #1.19123


if 'wli' in f_name or 'afm' in f_name:
    topography, general_settings = src.read.read_topography(f_name)
elif 'raman' in f_name:
    topography, general_settings = src.read.read_raman(f_name)

surf_area_linear, enlargement_factor_linear = src.calculate.surface_area(topography, general_settings)
surf_area_triangular, enlargement_factor_triangular = src.calculate.surface_area_triangular(topography, general_settings)

print(f'\nAnalyzed {f_name}:')
print(tabulate([['Linear', f'{surf_area_linear:5g}', f'{enlargement_factor_linear * 100:5g}'],
                ['Triangular', f'{surf_area_triangular:5g}', f'{enlargement_factor_triangular * 100:5g}']],
               headers=['Approach', 'Surf Area [µm^2]', 'Enlargement Factor [%]'], tablefmt='orgtbl'))
print(f'The projected area is{general_settings["width"]*general_settings["height"]:5g} µm^2.')

src.plot.plot_contourf(topography, general_settings)
