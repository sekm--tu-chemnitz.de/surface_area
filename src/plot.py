import matplotlib.pyplot as plt
from pandas import DataFrame
import numpy as np


def plot_contourf(df: DataFrame, general_settings: dict = None) -> None:
    n_x = df.shape[0]
    n_y = df.shape[1]

    if general_settings is None:
        total_width_x = 32
        total_width_y = 32
    else:
        total_width_x = general_settings['width']
        total_width_y = general_settings['height']

    x = np.linspace(0, total_width_x, n_y)
    y = np.linspace(0, total_width_y, n_x)
    x_mesh, y_mesh = np.meshgrid(x, y)

    plt.figure()
    cont = plt.contourf(x_mesh, y_mesh, df.to_numpy(), 100, cmap='afmhot')
    cbar = plt.colorbar(cont)
    plt.xlabel(r'$x\quad/\quad \mathrm{µm}$')
    plt.ylabel(r'$y\quad/\quad \mathrm{µm}$')
    cbar.set_label(r'$z\quad/\quad \mathrm{µm}$')
    plt.tight_layout()
    plt.show()


def subplot_contourf(df1: DataFrame, df2: DataFrame, general_settings1: dict = None, general_settings2: dict = None) \
        -> None:
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    '''Comparison plot: so far, only for Thermo-Kolloquium poster'''

    fig, (ax_left, ax_right) = plt.subplots(1, 2, figsize=cm2inch(38.66, 12.5))

    for df, general_settings, ax in zip([df1, df2], [general_settings1, general_settings2], [ax_left, ax_right]):
        n_x = df.shape[0]
        n_y = df.shape[1]

        if general_settings is None:
            total_width_x = 32
            total_width_y = 32
        else:
            total_width_x = general_settings['width']
            total_width_y = general_settings['height']

        x = np.linspace(0, total_width_x, n_y)
        y = np.linspace(0, total_width_y, n_x)
        x_mesh, y_mesh = np.meshgrid(x, y)

        cont = ax.contourf(x_mesh, y_mesh, df.to_numpy(), 100, cmap='afmhot')

        ax.tick_params(axis='both', which='major', labelsize=26)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size="7%", pad=0.4)
    cbar = fig.colorbar(cont, cax=cax)
    cbar.ax.tick_params(labelsize=26)
    cbar.set_label(r'$z\quad/\quad \mathrm{µm}$', size=26)

    plt.subplots_adjust(wspace=0.2, hspace=0.5, left=0.1, top=0.9, right=0.9, bottom=0.25)

    fig.text(0.5, 0.04, r'$x\quad/\quad \mathrm{µm}$', size=26, ha='center')
    fig.text(0.01, 0.5, r'$y\quad/\quad \mathrm{µm}$', size=26, va='center', rotation='vertical')

    plt.savefig('comparison.png')

    plt.show()


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)
