import pandas as pd
from pandas import read_csv, DataFrame
import re
import numpy as np


def read_topography(f_name: str) -> tuple:
    general_settings = read_header(f_name)
    try:
        df = read_csv(filepath_or_buffer=f'data/{f_name}', comment='#', delimiter='\t', decimal=',', index_col=False,
                      header=None, dtype=float)
    except ValueError:
        df = read_csv(filepath_or_buffer=f'data/{f_name}', comment='#', delim_whitespace=True, header=None, dtype=float)
    df = shift_to_zero(df)

    while df.isnull().any().any():
        df = df.apply(lambda x: x.fillna(x.mean()), axis=0)

    # Fit function to remove bended shape of the sinker
    y_mean = df.mean(axis=1)
    map_corr = np.stack([y_mean for _ in range(df.shape[1])]).transpose()
    df = df - map_corr

    df = shift_to_zero(df)
    df = convert_to_µm(df, general_settings)

    return df, general_settings


def convert_to_µm(df: DataFrame, general_settings: dict) -> DataFrame:
    if general_settings['units'] == 'm':
        return df*1E6
    elif general_settings['units'] == 'mm':
        return df*1E3


def shift_to_zero(df: DataFrame) -> DataFrame:
    return df-df.min().min()


def read_header(f_name: str) -> dict:
    general_settings = {}
    with open(f'data/{f_name}') as f:
        for line in f:
            if line.startswith('#'):
                if 'Width' in line or 'Breite' in line:
                    general_settings.update({'width': float(re.sub('[^\d\.]', '', line))})
                if 'Height' in line or 'Höhe' in line:
                    general_settings.update({'height': float(re.sub('[^\d\.]', '', line))})
                if 'Units' in line or 'Einheiten' in line:
                    general_settings.update({'units': line.split()[-1]})
    return general_settings


def read_raman(f_name: str) -> tuple:
    df = read_csv(filepath_or_buffer=f'data/{f_name}', comment='#', delimiter='\t', decimal='.', index_col=False,
                  header=None, dtype=float)
    matrix = np.zeros((int(max(df[0]) - min(df[0]) + 1), int(max(df[1]) - min(df[1]) + 1)))

    general_settings = {}
    general_settings.update({'width': max(df[0])-min(df[0])})
    general_settings.update({'height': max(df[1])-min(df[1])})
    general_settings.update({'units': 'µm'})

    with open(f'data/{f_name}') as file:
        file.readline()
        for line in file:
            matrix[int(float(line.split('\t')[0])-min(df[0])), int(float(line.split('\t')[1])-min(df[1]))] = float(line.split('\t')[2])

    return pd.DataFrame(matrix), general_settings
