import math
import numpy as np
import pandas as pd


def surface_area(df: pd.DataFrame, general_settings: dict = None) -> tuple:
    if general_settings is None:
        width_x = 32 / 512
        width_y = 32 / 512
        projected_area = 32 * 32
    else:
        width_x = general_settings['width'] / df.shape[0]
        width_y = general_settings['height'] / df.shape[1]
        projected_area = general_settings['width'] * general_settings['height']

    additional_surf_x = df.diff(axis=0).abs().sum() * width_x
    cum_area_x = additional_surf_x.abs().sum()
    additional_surf_y = df.diff(axis=1).abs().sum() * width_y
    cum_area_y = additional_surf_y.abs().sum()

    cumulated_area = cum_area_x + cum_area_y + projected_area

    return cumulated_area, cumulated_area / projected_area


def surface_area_triangular(df: pd.DataFrame, general_settings: dict = None) -> tuple:
    df_extended = pd.concat([df.iloc[:, 0], df], ignore_index=True, axis=1)
    df_extended = pd.concat([df_extended, df.iloc[:, -1]], ignore_index=True, axis=1)
    df_extended.loc[-1] = df_extended.loc[0, :]
    df_extended.index = df_extended.index + 1
    df_extended.sort_index(inplace=True)

    if general_settings is None:
        width_x = 32 / 512
        width_y = 32 / 512
        projected_area = 32 * 32

    else:
        width_x = general_settings['width'] / df.shape[0]
        width_y = general_settings['height'] / df.shape[1]
        projected_area = general_settings['width'] * general_settings['height']

    cumulated_area = 0
    for x_i, x in enumerate(df_extended.itertuples(), 0):
        for y_i, y in enumerate(x[:-1], 0):
            if 1 > x_i or 1 > y_i:
                continue
            x_center = x_i * width_x
            y_center = y_i * width_y
            x_left = (x_i - 1) * width_x
            y_up = (y_i - 1) * width_y

            pos_center = np.array([x_center, y_center, df_extended.iloc[x_i, y_i]])
            pos_upper_left = np.array([x_left, y_up, df_extended.iloc[x_i-1, y_i-1]])
            pos_upper_center = np.array([x_center, y_up, df_extended.iloc[x_i, y_i-1]])
            pos_left_center = np.array([x_left, y_center, df_extended.iloc[x_i - 1, y_i]])

            cumulated_area += areatriangle3d(pos_center, pos_upper_left, pos_upper_center)
            cumulated_area += areatriangle3d(pos_center, pos_upper_left, pos_left_center)

    return cumulated_area, cumulated_area / projected_area


def heron(a: float, b: float, c: float) -> float:
    s = (a + b + c) / 2
    return math.sqrt(s * (s - a) * (s - b) * (s - c))


def distance3d(pos_a: np.array, pos_b: np.array) -> float:
    return np.linalg.norm(pos_a-pos_b)


def areatriangle3d(pos_a: np.array, pos_b: np.array, pos_c: np.array) -> float:
    a = distance3d(pos_a=pos_a, pos_b=pos_b)
    b = distance3d(pos_a=pos_b, pos_b=pos_c)
    c = distance3d(pos_a=pos_c, pos_b=pos_a)
    area = heron(a, b, c)
    return area
